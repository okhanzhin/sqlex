SELECT name, CONVERT(CHAR(10), DATE, 120) AS battle_dt,
CONVERT(CHAR(10), MIN(DATEADD(dd,1,dt)), 120) AS election_dt
FROM(
SELECT name, date, DATEADD(yy, p, DATEADD(dd, n, DATEADD(mm, 3, DATEADD(yy, DATEDIFF(yy, 0, date), 0)))) AS dt
FROM Battles
,(VALUES(0), (1), (2), (3), (4), (5), (6), (7), (8)) T(p)
,(VALUES(0), (1), (2), (3), (4), (5), (6)) W(n) ) X
WHERE date <= dt AND (Year(dt) % 4 = 0 AND Year(dt) % 100 > 0 OR Year(dt) % 400 = 0)
and DATEPART(dw, dt) = DATEPART(dw, '20140106')
GROUP BY name, date;