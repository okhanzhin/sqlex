DELETE FROM Ships
FROM Outcomes as o INNER JOIN Ships as s
ON o.ship = s.name
WHERE o.result = 'sunk';
