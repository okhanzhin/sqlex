SELECT DISTINCT type, laptop.model, speed FROM product, laptop
WHERE type = 'laptop' AND speed < (SELECT MIN(speed) FROM PC);