WITH CTE AS (
SELECT 1 n, CAST (0 AS varchar(16)) bit_or,
code, speed, ram FROM PC
UNION ALL
SELECT n * 2,
CAST (CONVERT(bit, (speed|ram)&n) AS VARCHAR(1)) + 
CAST(bit_or AS VARCHAR(15)), code, speed, ram
FROM CTE WHERE n < 65536)
SELECT code, speed, ram FROM CTE
WHERE n = 65536
AND CHARINDEX('1111', bit_or) > 0;