SELECT TOP 1 WITH TIES name, c3 FROM passenger 
JOIN (SELECT c1, max(c3) c3 FROM (
SELECT pass_in_trip.ID_psg c1, Trip.ID_comp c2, COUNT(*) c3 
FROM pass_in_trip 
JOIN trip ON trip.trip_no = pass_in_trip.trip_no 
GROUP BY pass_in_trip.ID_psg, Trip.ID_comp ) AS t
GROUP BY c1 HAVING COUNT(*) = 1) AS tt ON ID_psg = c1 
ORDER BY c3 DESC;