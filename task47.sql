WITH T1 AS ( SELECT COUNT(name) as co, country FROM
(SELECT name, country FROM Classes INNER JOIN Ships ON Ships.class = Classes.class
UNION
SELECT ship, country FROM Classes INNER JOIN Outcomes ON Outcomes.ship = Classes.class) FR1
GROUP BY country
),
T2 AS ( SELECT COUNT(name) as co, country FROM ( SELECT name, country FROM Classes INNER JOIN Ships ON Ships.class = Classes.class
WHERE name IN (SELECT DISTINCT ship FROM Outcomes WHERE result LIKE 'sunk')
UNION
SELECT ship, country FROM Classes INNER JOIN Outcomes ON Outcomes.ship = Classes.class
WHERE ship IN (SELECT DISTINCT ship FROM Outcomes WHERE result LIKE 'sunk')
) FR2 GROUP BY country )
SELECT T1.country FROM T1
INNER JOIN T2 ON T1.co = t2.co and t1.country = t2.country;
