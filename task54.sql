SELECT CAST(AVG(numguns * 1.0) AS NUMERIC (6,2)) as AVG_nmg 
FROM (SELECT ship, type, numguns   FROM Outcomes 
LEFT JOIN Classes ON ship = class  
UNION  
SELECT name, type, numguns FROM Ships AS s 
INNER JOIN  Classes AS c ON c.class = s.class ) AS T 
WHERE type = 'bb';
