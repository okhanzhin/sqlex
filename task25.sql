SELECT DISTINCT maker 
FROM Product 
WHERE type = 'printer' AND maker IN (
SELECT maker FROM Product WHERE model IN (
SELECT model FROM pc WHERE speed = (
SELECT MAX(speed) FROM (
SELECT speed FROM pc WHERE ram = (
SELECT MIN(ram) FROM PC)) as z4 ) 
AND ram = (SELECT MIN(ram) FROM pc)));