INSERT INTO pc (code, model, speed, ram, hd, cd, price)
SELECT p.code + mod.model AS code,
		mod.model, speed, ram, hd, cd, price
FROM
(SELECT MAX(code) AS code, MAX(speed) AS speed, MAX(ram) AS ram, MAX(hd) AS hd,
CAST(MAX(CAST(LEFT(cd, CHARINDEX('x',cd) - 1) AS INT)) AS VARCHAR(10)) + 'x' AS cd,
AVG(price) AS price FROM pc) AS p,
(SELECT model FROM product WHERE type = 'pc'
EXCEPT
SELECT model FROM pc) as mod;