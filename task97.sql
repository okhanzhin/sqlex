WITH r AS
(SELECT row_number() OVER(PARTITION BY code ORDER BY code,feat) num, feat, code FROM (
SELECT speed feat, code FROM laptop
UNION ALL
SELECT ram, code FROM laptop
UNION ALL
SELECT price, code FROM laptop
UNION ALL
SELECT screen, code FROM laptop) aa)
SELECT code, speed, ram, price, screen FROM laptop WHERE code IN (
SELECT code FROM (SELECT [code],[1],[2],[3],[4] FROM r x pivot 
(MIN(feat) FOR NUM IN ([1],[2],[3],[4])) pvt) aa 
WHERE [4]/[3] >= 2 AND [3]/[2] >= 2 AND [2]/[1] >= 2);