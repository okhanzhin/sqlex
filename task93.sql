SELECT c.name, SUM(vr.vr) FROM (
SELECT DISTINCT t.id_comp, pt.trip_no, pt.date, t.time_out, t.time_in, --pt.id_psg,
CASE
WHEN DATEDIFF(mi, t.time_out, t.time_in) > 0 THEN DATEDIFF(mi, t.time_out, t.time_in)
WHEN DATEDIFF(mi, t.time_out, t.time_in) <= 0 THEN DATEDIFF(mi, t.time_out, t.time_in + 1)
END vr
FROM pass_in_trip pt LEFT JOIN trip t ON pt.trip_no = t.trip_no) vr 
LEFT JOIN company c ON vr.id_comp = c.id_comp
GROUP BY c.name;