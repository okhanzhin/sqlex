INSERT INTO pc(code, model, speed, ram, hd, cd, price)
SELECT MIN(laptop.code) + 20,
laptop.model + 1000,
MAX(laptop.speed),
MAX(laptop.ram) * 2,
MAX(laptop.hd) * 2,
CAST(MAX(CAST(LEFT(cd, CHARINDEX('x',cd) - 1) AS INT)) AS VARCHAR(10)) + 'x',
MAX(laptop.price) / 1.5
FROM laptop, pc
WHERE laptop.model IN (SELECT model FROM laptop)
GROUP BY laptop.model
