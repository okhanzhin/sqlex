SELECT DISTINCT maker FROM pc INNER JOIN product
ON pc.model = product.model WHERE pc.speed >= 750 
AND maker IN (SELECT maker FROM laptop INNER JOIN product
ON laptop.model = product.model WHERE laptop.speed >= 750);