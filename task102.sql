SELECT name FROM passenger
WHERE ID_psg IN (
SELECT ID_psg FROM trip t, pass_in_trip pit
WHERE t.trip_no = pit.trip_no
GROUP BY ID_psg
HAVING COUNT(DISTINCT CASE WHEN town_from <= town_to 
THEN town_from + town_to 
ELSE town_to + town_from 
END) = 1);