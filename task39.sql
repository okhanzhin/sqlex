SELECT DISTINCT B.ship 
FROM(SELECT * FROM outcomes LEFT JOIN battles ON battle = name WHERE RESULT = 'damaged')AS B 
WHERE EXISTS (SELECT ship FROM outcomes LEFT JOIN battles ON battle = name 
WHERE ship = B.ship AND B.date < date);