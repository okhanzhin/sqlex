SELECT income.point, income.date, 'inc' AS operation, SUM(income.inc) 
FROM income LEFT JOIN outcome ON 
income.point = outcome.point AND income.date=outcome.date 
WHERE outcome.date IS NULL  GROUP BY income.point, income.date 
UNION 
SELECT outcome.point, outcome.date, 'out' AS operation, SUM(outcome.out) 
FROM income RIGHT JOIN outcome ON 
income.point = outcome.point AND income.date = outcome.date 
WHERE income.date IS NULL 
GROUP BY outcome.point, outcome.date; 