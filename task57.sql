SELECT class AS class, COUNT(class) AS sunked FROM( 
SELECT C.class, O.ship FROM classes AS c 
JOIN outcomes AS o ON c.class = o.ship 
WHERE o.result = 'sunk' 
UNION 
SELECT s.class, o.ship FROM outcomes AS o 
JOIN ships AS s ON s.name = o.ship WHERE o.result = 'sunk') AS T 
WHERE class IN (
SELECT DISTINCT x.class FROM (
SELECT c.class, o.ship FROM classes AS c 
JOIN outcomes AS o ON c.class = o.ship 
UNION 
SELECT c.class, s.name FROM classes AS c 
JOIN ships AS s ON c.class = s.class) AS x 
GROUP BY x.class 
HAVING COUNT(x.class) >= 3 ) GROUP BY class;
