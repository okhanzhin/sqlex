SELECT class FROM (SELECT name, class FROM ships
UNION
SELECT class AS name, class FROM classes, outcomes WHERE outcomes.ship = classes.class) t1
GROUP By class HAVING COUNT(t1.name) = 1;
