SELECT a.point, CASE 
WHEN o IS NULL  THEN i 
ELSE i-o 
END 
remain FROM (select point, SUM(inc) AS i 
FROM Income_o WHERE '20010415' > date GROUP BY point) AS A 
LEFT JOIN (SELECT point, SUM(out) AS o 
FROM Outcome_o  WHERE '20010415' > date GROUP BY point) AS B on A.point=B.point;
