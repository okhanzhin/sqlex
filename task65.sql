SELECT row_number() OVER (order by maker) num, maker2 maker, type from (
SELECT maker, maker2 =
CASE row_number() over (PARTITION BY maker ORDER BY CASE
WHEN type = 'PC' THEN 1
WHEN type = 'Laptop' THEN 2
WHEN type = 'Printer' THEN 3
ELSE 4
END)
WHEN 1 THEN maker
ELSE '' 
END
, type FROM (SELECT DISTINCT maker, type FROM product) x) y;
 