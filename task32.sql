SELECT country, CAST(AVG(bore*bore*bore/2) AS NUMERIC(6,2)) FROM
(SELECT country, name, bore FROM ships sh JOIN classes c ON sh.class = c.class
UNION
SELECT country, ship, bore FROM outcomes o JOIN classes c ON o.ship = c.class) t1
GROUP BY country;