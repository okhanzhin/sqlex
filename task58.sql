SELECT main_maker, main_type, CONVERT(NUMERIC(6,2), 
((sub_num * 100.00) / (total_num * 100.00) * 100.00)) FROM (
SELECT COUNT(p5.model) total_num ,p5.maker main_maker
FROM product p5 GROUP BY p5.maker) p6 JOIN (
SELECT p3.maker sub_maker ,p3.type main_type ,COUNT(p4.model) sub_num FROM (
SELECT p1.maker maker, p2.type type FROM product p1 
CROSS JOIN product p2 GROUP BY p1.maker, p2.type) p3 
LEFT JOIN product p4 ON p3.maker = p4.maker AND p3.type = p4.type 
GROUP BY p3.maker,p3.type) p7 ON p7.sub_maker = p6.main_maker;