SELECT a.point, CASE 
WHEN o IS NULL THEN i 
ELSE i-o 
END 
remain FROM (SELECT point, SUM(inc) AS i 
FROM Income_o GROUP BY point) AS A 
LEFT JOIN (
SELECT point, sum(out) AS o FROM Outcome_o 
GROUP BY point) AS B ON A.point = B.point;