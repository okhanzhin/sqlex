SELECT classes.class, COUNT(T.ship) FROM classes 
LEFT JOIN (SELECT ship, class FROM outcomes 
LEFT JOIN ships ON ship = name 
WHERE result = 'sunk' 
UNION 
SELECT ship, class FROM outcomes 
LEFT JOIN classes ON ship = class WHERE result='sunk') as T 
ON classes.class = T.class GROUP BY classes.class;
