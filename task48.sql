SELECT class AS class_name FROM ships
WHERE name IN (
SELECT ship FROM outcomes WHERE result = 'sunk')
UNION
SELECT ship AS class_name FROM outcomes
WHERE ship NOT IN (SELECT name FROM ships) AND
ship IN (SELECT class FROM classes) AND result = 'sunk';