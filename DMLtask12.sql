UPDATE laptop
SET
screen = screen + 1,
price = price - 100
FROM laptop LEFT JOIN product
ON laptop.model = product.model
WHERE product.maker IN ('E', 'B');