SELECT top 1 with ties country, x, n
FROM classes
CROSS apply(VALUES(numguns*5000, 'numguns'), (bore*3000,'bore'), (displacement,'displacement')) V(x,n)
GROUP BY country, x, n
ORDER BY rank() over(PARTITION BY country ORDER BY x DESC)