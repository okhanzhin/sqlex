SELECT DISTINCT name FROM ships, classes 
WHERE ships.class = classes.class AND launched >= 1922 AND type = 'bb' AND displacement > 35000;