WITH a AS(
SELECT x.class, x.numGuns, ROW_NUMBER() OVER (PARTITION BY x.class ORDER BY x.numguns)n
FROM Classes x, classes y
WHERE x.type = 'bc')
SELECT DISTINCT class,'bc-' + CAST(n AS CHAR(2))
FROM a WHERE numguns >= n;