DELETE FROM Laptop
FROM product INNER JOIN laptop 
ON product.model = laptop.model
WHERE maker IN (
SELECT DISTINCT maker 
FROM product WHERE type = 'laptop'
EXCEPT 
SELECT DISTINCT product.maker
FROM product
WHERE type = 'printer');