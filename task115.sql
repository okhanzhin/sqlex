SELECT DISTINCT Up = u.b_vol, Down = d.b_vol, Side = s.b_vol,
Rad = CAST(POWER((POWER(s.b_vol, 2) - POWER((1.*d.b_vol - 1.*u.b_vol) / 2,2)),1./2.) / 2 AS DEC(15,2))
FROM utB u, utB d, utB s
WHERE u.b_vol < d.b_vol AND 1.*u.b_vol + 1.*d.b_vol = 2.*s.b_vol;