SELECT TOP 1 WITH TIES * FROM (
SELECT COUNT(DISTINCT(pt.trip_no)) qty, pt.date
FROM Trip t, Pass_in_trip pt
WHERE t.trip_no = pt.trip_no AND t.town_from = 'Rostov'
GROUP BY pt.date) t1
ORDER BY t1.qty DESC;