SELECT DISTINCT name FROM ships 
INNER JOIN classes c ON ships.class = c.class 
WHERE (numGuns >= 9 OR numguns IS NULL) AND 
(bore < 19 OR bore IS NULL) AND 
(displacement <= 65000 OR displacement IS NULL) AND 
type = 'bb' AND country = 'japan'; 