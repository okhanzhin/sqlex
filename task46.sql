SELECT name AS name, displacement AS displacement, numguns AS numGuns FROM ships INNER JOIN classes ON ships.class=classes.class WHERE name IN (SELECT ship FROM outcomes WHERE battle = 'Guadalcanal')   
UNION 
SELECT ship AS name, null AS displacement, null AS numGuns FROM outcomes WHERE battle = 'Guadalcanal' AND ship NOT IN (SELECT name FROM ships) AND ship NOT IN  (SELECT class FROM classes)   
UNION  
SELECT ship AS name, displacement AS displacement, numguns AS numGuns FROM outcomes INNER JOIN classes ON outcomes.ship=classes.class WHERE battle = 'Guadalcanal' AND ship NOT IN (SELECT name FROM ships);