SELECT maker, COUNT(maker) FROM product GROUP BY maker  HAVING COUNT(maker) IN ( 
SELECT MAX(D.cnt) FROM (
SELECT maker, COUNT(maker) AS cnt FROM product GROUP BY maker) AS D
UNION
SELECT MIN(F.cnt) FROM (
SELECT maker, COUNT(maker) AS cnt FROM product GROUP BY maker) AS F);