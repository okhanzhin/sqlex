SELECT CAST(AVG(a1) AS NUMERIC(6,2)) AS avg_paint
FROM (SELECT CASE WHEN SUM(b_vol) IS NULL 
THEN 0
ELSE CAST(SUM(b_vol) AS NUMERIC(6,2)) 
END a1
FROM utq LEFT JOIN utb ON utq.q_id = utb.b_q_id
GROUP BY q_id) x;
