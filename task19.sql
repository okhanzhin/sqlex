SELECT t1.maker, avg(lt.screen)
FROM product t1
INNER JOIN laptop lt ON lt.model = t1.model
WHERE t1.type LIKE 'Laptop'
GROUP BY t1.maker;