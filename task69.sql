WITH op AS (SELECT point, [date], Inc AS sm FROM Income
UNION ALL
SELECT point, [date], -Out FROM Outcome)
SELECT point, CONVERT(CHAR(25),[date],103) AS [date],
(SELECT SUM(sm) FROM op
WHERE op.[date] <= o1.[date] AND op.point = o1.point) AS rem
FROM op AS o1
GROUP BY point, [date];
