SELECT COUNT(t1) AS qty FROM (
SELECT town_from AS t1, town_to, COUNT(plane) AS cp FROM Trip 
GROUP BY town_from, town_to 
HAVING COUNT(plane) >= ALL(
SELECT COUNT(plane) FROM Trip 
GROUP BY town_from, town_to) ) AS tab; 