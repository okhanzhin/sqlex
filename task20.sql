SELECT  maker, COUNT(model) AS count_model
FROM product  
WHERE type = 'pc'
GROUP BY maker HAVING COUNT(model) >=3;