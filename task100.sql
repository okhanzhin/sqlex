SELECT DISTINCT A.date , A.R, B.point, B.inc, C.point, C.out
FROM (SELECT DISTINCT date, ROW_Number() OVER(PARTITION BY date ORDER BY code asc) AS R FROM Income
UNION 
SELECT DISTINCT date, ROW_Number() OVER(PARTITION BY date ORDER BY code asc) FROM Outcome) A
LEFT JOIN (
SELECT date, point, inc, 
ROW_Number() OVER(PARTITION BY date ORDER BY code asc) AS RI FROM Income) B ON B.date = A.date AND B.RI = A.R
LEFT JOIN (
SELECT date, point, out, 
ROW_Number() OVER(PARTITION BY date ORDER BY code asc) AS RO FROM Outcome) C ON C.date = A.date AND C.RO = A.R;