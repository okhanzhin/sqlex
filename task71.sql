SELECT p.maker FROM product p 
WHERE p.type = 'pc' 
GROUP BY p.maker 
HAVING COUNT(DISTINCT p.model) = (
SELECT COUNT(DISTINCT pc.model) FROM pc 
WHERE pc.model IN (
SELECT DISTINCT pr.model FROM product pr 
WHERE pr.maker = p.maker)); 