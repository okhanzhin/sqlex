SELECT maker, AVG(HD) FROM product INNER JOIN PC 
ON pc.model = product.model
WHERE maker IN (SELECT maker FROM product WHERE type = 'printer')
GROUP BY maker;